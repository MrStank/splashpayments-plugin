/**
 * Adds a function to jQuery that allows use to easily load an external script, and invoke a callback on success or error.
 * 
 * @param  {String} url
 * @param  {Function} successCallback
 * @param  {Function} errorCallback
 */
jQuery.loadScript = function(url, successCallback, errorCallback)
{
    jQuery.ajax(
    {
        url: url,
        dataType: "script",
        success: successCallback,
        error: errorCallback,
        async: true
    });
}

/**
 * Initialize the checkout script.
 */
function init()
{
    // Make sure that our params set in the backend exist before using them everywhere.
    if(wc_splash_payments_request_params === null || wc_splash_payments_request_params === undefined)
    {
        return;
    }

    // Choose the correct PayFields URL, based on testing status.
    let payFieldsUrl = wc_splash_payments_request_params.isTesting ?
        "https://test-api.splashpayments.com/payFieldsScript" : "https://api.splashpayments.com/payFieldsScript";
    
    // Load PayFields script.
    jQuery.loadScript(payFieldsUrl, payFieldsLoaded);
}

/**
 * Returns the current selected payment method based on the checkbox.
 * Not gonna lie, this is a bit of a hack. However, it works.
 */
function getPaymentMethod()
{
    // Define a constant for a "other" payment method.
    const otherPaymentMethodLiteral = "Other";

    // Get Splash Payments credit card payment method.
    let creditCardPaymentMethod = jQuery("#payment_method_" + wc_splash_payments_request_params.paymentMethods.CC);

    // If the Splash Payments credit card payment method wasn't found, return another payment method.
    if(creditCardPaymentMethod === null || creditCardPaymentMethod === undefined)
    {
        return otherPaymentMethodLiteral;
    }

    // Return the Splash Payments credit card payment method if it is checked.
    // Return another payment method if not.
    return creditCardPaymentMethod[0].checked ? wc_splash_payments_request_params.paymentMethods.CC : otherPaymentMethodLiteral;
}

/**
 * Callback that is invoked when the PayFields script is loaded.
 */
function payFieldsLoaded()
{
    // Create an object that contains all of our functionality in one, scoped, non-global place.
    let wc_splash_payments_request =
    {
        /**
         * Sets up the Splash Payments PayFields script.
         */
        setupPayFields: function()
        {
            // Set PayFields authentication credentials.
            PayFields.config.apiKey = wc_splash_payments_request_params.api_key;
            PayFields.config.merchant = wc_splash_payments_request_params.merchant_id;
            PayFields.config.amount = wc_splash_payments_request_params.order.total.amount;
            
            // Guide the PayFields to find the elements used for the input fields.
            PayFields.fields =
            [
                { type: "number", element: "#number" },
                { type: "cvv", element: "#cvv" },
                { type: "name", element: "#name" },
                { type: "address", element: "#address" },
                { type: "expiration", element: "#expiration" }
            ];
            
            // Wait one second before appending the iFrame.
            // Why, you ask, if this required? I haven't a clue...
            setTimeout(function()
            {
                PayFields.appendIframe();
            }, 1000);
        },

        /**
         * Make a payment request using the PayFields script.
         */
        paymentRequest: function()
        {
            PayFields.submit();
        },

        /**
         * Override the default WooCommerce payment submission so that it uses Splash Payments' PayFields.
         */
        overrideDefaultSubmission: function()
        {
            // Use jQuery to find the place order button.
            // Then, either use PayFields for payment, or use the default WooCommerce functionality.
            jQuery(document).on("click", "#place_order", function(e)
            {
                // Retrieve the selected payment method.
                let paymentMethod = getPaymentMethod();

                // If the current payment method is Splash Payments Credit Card, pay with the PayFields script.
                if(paymentMethod === wc_splash_payments_request_params.paymentMethods.CC)
                {
                    e.preventDefault();
                    wc_splash_payments_request.paymentRequest();
                }
                else // If the payment method is anything else, use the default WooCommerce action.
                {
                    return true;
                }
            });
        },

        /**
         * Adds the functionality used for each callback in the PayFields script.
         */
        registerCallbacks: function()
        {
            // Invoked when payment is successful.
            PayFields.onSuccess = function(response)
            {
                // Set our hidden input's value to be equal to the current transaction's ID.
                let checkout_form = jQuery("form.woocommerce-checkout");
                checkout_form.find("#_transID").val(response.data[0].id);

                // Submit the form.
                checkout_form.submit();

                // Let the console know that everything was all good.
                console.log("Payment Successful.");
            };
            
            // Invoked when payment is not successful.
            // An example is entering the incorrect credit card number.
            PayFields.onFailure = function(response)
            {
                // Let the console know that the transaction failed.
                console.warn("Transaction failed.");
            };
            
            // Invoked when the input fields were not correctly validated.
            // An example is leaving the credit card number blank.
            PayFields.onValidationFailure = function()
            {
                // Let the console know that the inputs could not be validated.
                console.warn("Payment inputs fields could not be validated.");
            };

            // Invoked when the payment has been completely processed by Splash Payments through the PayFields script.
            // This callback gets invoked regardless if the payment was a success OR a failure.
            PayFields.onFinish = function(response)
            {
                // Let the console know that the transaction finished, whether it was a success or not.
                console.log("Transaction finished.");
            };
        },

        /**
         * Initialize all of the functionality used by the PayFields script.
         */
        init: function()
        {
            // Prepare the PayFields scripts.
            // This includes setting authentication, finding the correct inputs, and rendering the iFrame.
            this.setupPayFields();

            // Overide the default WooCommerce action when the "Place Order" button is clicked.
            this.overrideDefaultSubmission();
            
            // Add our functionality for what happens during callbacks that are invoked from the PayFields script.
            this.registerCallbacks();
        }
    };

    // Initialize our payment method using Splash Payments' PayFields script.
    wc_splash_payments_request.init();
}

// Start our checkout script.
init();
