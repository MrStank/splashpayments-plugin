<?php
/**
 * Plugin Name: WooCommerce Splash Payments
 * Version: 1.0.0
 * Plugin URI:
 * Description: Adds ability to make purchases with Splash Payments.
 * Author: CloudMedia
 * Author URI: https://cloudmedia.agency/
 * Text Domain: wc_splash_payments
 *
 * @package WordPress
 * @author CloudMedia
 */

//Exit if accessed directly
if( !defined( 'ABSPATH' ) )
{
    exit;
}

if( !class_exists( 'WC_Splash_Payments' ) ) :

    define( 'WC_SPLASH_PAYMENTS_VERSION', '1.0.0' );

    /**
     * Main class.
     *
     * @package WC_Splash_Payments
     * @since 1.0.0
     * @version 1.0.0
     */
    class WC_Splash_Payments
    {
        private static $_instance = null;

        /**
         * Get the single instance aka Singleton.
         *
         * @access public
         * @since 1.0.0
         * @version 1.0.0
         * @return WC_Splash_Payments
         */
        public static function instance()
        {
            if( is_null( self::$_instance ) )
            {
                self::$_instance = new self();
            }

            return self::$_instance;
        }

        /**
         * Prevent cloning.
         *
         * @access public
         * @since 1.0.0
         * @version 1.0.0
         * @return bool
         */
        public function __clone()
        {
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wc_splash_payments' ), WC_SPLASH_PAYMENTS_VERSION );
        }

        /**
         * Prevent unserializing instances
         *
         * @access public
         * @since 1.0.0
         * @version 1.0.0
         * @return bool
         */
        public function __wakeup()
        {
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wc_splash_payments' ), WC_SPLASH_PAYMENTS_VERSION );
        }

        /**
         * WC_Splash_Payments constructor.
         */
        private function __construct()
        {
            add_action( 'woocommerce_loaded', array( $this, 'bootstrap' ) );
        }

        public function bootstrap()
        {
            $this->define_constants();
            $this->includes();
            $this->init();
            $this->init_hooks();

            do_action( 'wc_splash_payments_loaded' );
        }

        /**
         * Registers settings for WooCommerce Splash Payments.
         *
         * @access public
         * @since 1.0.0
         * @version 1.0.0
         * @return void
         */
        function register_settings()
        {
            //Add options
            add_option(
                WC_SPLASH_PAYMENTS_SETTINGS_PRIVATE_API_KEY
            );

            add_option(
                WC_SPLASH_PAYMENTS_SETTINGS_PUBLIC_API_KEY
            );

            add_option(
                WC_SPLASH_PAYMENTS_SETTINGS_MERCHANT_ID
            );

            add_option(
                WC_SPLASH_PAYMENTS_SETTINGS_TESTING
            );
        }

        public function check_environment()
        {
            $private_api_key = WC_Splash_Payments_Utils::getPrivateAPIKey();
            $public_api_key  = WC_Splash_Payments_Utils::getPublicAPIKey();
            $merchant_id     = WC_Splash_Payments_Utils::getMerchantID();

            //If there is no API Key set
            if( empty( $private_api_key ) || empty( $public_api_key ) )
            {
                ?>

                <div class="error">
                    <p>
                        <strong>
                            <?php _e( 'WooCommerce Splash Payments needs both API Keys set. ', 'wc_splash_payments' ); ?>
                            <a href="<?php echo admin_url( 'admin.php?page=wc-settings&tab=checkout&section=splash_payments_cc' ); ?>">
                                <?php _e( 'Set it here.', 'wc_splash_payments' ); ?>
                            </a>
                        </strong>
                    </p>
                </div>

                <?php
            }

            //If there is no Merchant ID set
            if( empty( $merchant_id ) )
            {
                ?>

                <div class="error">
                    <p>
                        <strong>
                            <?php _e( 'WooCommerce Splash Payments needs a Merchant ID set. ', 'wc_splash_payments' ); ?>
                            <a href="<?php echo admin_url( 'admin.php?page=wc-settings&tab=checkout&section=splash_payments_cc' ); ?>">
                                <?php _e( 'Set it here.', 'wc_splash_payments' ); ?>
                            </a>
                        </strong>
                    </p>
                </div>

                <?php
            }
        }

        /**
         * Define constants.
         *
         * @access public
         * @since 1.0.0
         * @version 1.0.0
         * @return void
         */
        public function define_constants()
        {
            define( 'WC_SPLASH_MAIN_FILE', __FILE__ );
            define( 'WC_SPLASH_PAYMENTS_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
            define( 'WC_SPLASH_PAYMENTS_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );

            //Settings Keys
            define( 'WC_SPLASH_PAYMENTS_SETTINGS_PRIVATE_API_KEY', 'wc_splash_payments_option_private_api_key' );
            define( 'WC_SPLASH_PAYMENTS_SETTINGS_PUBLIC_API_KEY', 'wc_splash_payments_option_public_api_key' );
            define( 'WC_SPLASH_PAYMENTS_SETTINGS_MERCHANT_ID', 'wc_splash_payments_option_merchant_id' );
            define( 'WC_SPLASH_PAYMENTS_SETTINGS_TESTING', 'wc_splash_payments_option_testing' );
        }

        /**
         * Include all files needed
         *
         * @access public
         * @since 1.0.0
         * @version 1.0.0
         * @return void
         */
        public function includes()
        {
            //Splash Payments SDK
            require_once dirname( __FILE__ ) . '/includes/sdk/splashpayments-php-1.0.7/vendor/autoload.php';

            //Composer
            require_once dirname( __FILE__ ) . '/includes/vendor/autoload.php';

            //Utility class
            require_once dirname( __FILE__ ) . '/includes/class-wc-splash-payments-utils.php';

            //Abstract Classes
            require_once dirname( __FILE__ ) . '/includes/abstracts/abstract-wc-splash-payments-gateway_payment.php';

            //Payment Methods
            require_once dirname( __FILE__ ) . '/includes/payment-methods/class-wc-splash-payments-payment-request.php';
            require_once dirname( __FILE__ ) . '/includes/payment-methods/class-wc-splash-payments-gateway-cc.php';
            require_once dirname( __FILE__ ) . '/includes/payment-methods/class-wc-splash-payments-gateway-echeck.php';
        }

        /**
         * Initializes the plugin.
         *
         * @access public
         * @since 1.0.0
         * @version 1.0.0
         * @return void
         */
        public function init()
        {
            //Set testing mode
            \SplashPayments\Utilities\Config::setTestMode( WC_Splash_Payments_Utils::getTestingStatus() );
        }

        /**
         * Initializes hooks
         *
         * @access public
         * @since 1.0.0
         * @version 1.0.0
         * @return void
         */
        public function init_hooks()
        {
            register_activation_hook( __FILE__, array( 'WC_Splash_Payments_Deactivation', 'deactivate' ) );

            if( class_exists( 'WooCommerce' ) )
            {
                add_filter( 'woocommerce_payment_gateways', array( $this, 'wc_splash_payments_add_gateway' ) );

                add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'wc_splash_payments_plugin_links' ) );

                add_action( 'admin_init', array( $this, 'register_settings' ) );
                add_action( 'admin_notices', array( $this, 'check_environment' ) );
            }
            else
            {
                add_action( 'admin_notices', array( $this, 'woocommerce_missing_notice' ) );
            }
        }

        /**
         * Add the gateway to WC Available Gateways
         *
         * @since 1.0.0
         * @param array $gateways all available WC gateways
         * @return array $gateways all WC gateways + Splash Payments gateway
         */
        function wc_splash_payments_add_gateway( $gateways )
        {
            //Add Splash Payments payment gateways
            $gateways[] = 'WC_Splash_Payments_Gateway_CC';
            $gateways[] = 'WC_Splash_Payments_Gateway_eCheck';

            return $gateways;
        }

        /**
         * Adds plugin page links
         *
         * @since 1.0.0
         * @param array $links all plugin links
         * @return array $links all plugin links + our custom links (i.e., "Settings")
         */
        function wc_splash_payments_plugin_links( $links )
        {
            $plugin_links = array(
                '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=splash_payments_cc' ) . '">'
                . __( 'Settings', 'wc_splash_payments' ) .
                '</a>'
            );
            return array_merge( $plugin_links, $links );
        }

        /**
         * WooCommerce fallback notice.
         *
         * @access public
         * @since 1.0.0
         * @version 1.0.0
         * @return bool
         */
        public function woocommerce_missing_notice()
        {
            ?>

            <div class="error">
                <p>
                    <?php _e( 'WooCommerce Splash Payments Plugin requires WooCommerce to be installed and activated. You can download it ', 'wc_splash_payments' ); ?>
                    <a href="https://woocommerce.com/woocommerce/" target="_blank"
                       rel="nofollow"><?php _e( 'here.', 'wc_splash_payments' ); ?></a>
                </p>
            </div>

            <?php

            return true;
        }
    }

    WC_Splash_Payments::instance();

endif;