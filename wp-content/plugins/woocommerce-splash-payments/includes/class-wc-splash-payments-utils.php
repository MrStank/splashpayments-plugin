<?php
//Exit if accessed directly
if( !defined( 'ABSPATH' ) )
{
    exit;
}

if( !class_exists( 'WC_Splash_Payments_Utils' ) ) :

    class WC_Splash_Payments_Utils
    {
        /**
         * Set the Splash API key to the Public Key.
         *
         * @since 1.0.0
         * @return void
         */
        public static function setAPIPublic()
        {
            \SplashPayments\Utilities\Config::setApiKey( get_option( WC_SPLASH_PAYMENTS_SETTINGS_PUBLIC_API_KEY ) );
        }

        /**
         * Set the Splash API key to the Private Key.
         *
         * @since 1.0.0
         * @return void
         */
        public static function setAPIPrivate()
        {
            \SplashPayments\Utilities\Config::setApiKey( get_option( WC_SPLASH_PAYMENTS_SETTINGS_PRIVATE_API_KEY ) );
        }

        /**
         * Retrieve the Private API Key.
         *
         * @since 1.0.0
         * @return string
         */
        public static function getPrivateAPIKey()
        {
            return get_option( WC_SPLASH_PAYMENTS_SETTINGS_PRIVATE_API_KEY );
        }

        /**
         * Retrieve the Public API Key.
         *
         * @since 1.0.0
         * @return string
         */
        public static function getPublicAPIKey()
        {
            return get_option( WC_SPLASH_PAYMENTS_SETTINGS_PUBLIC_API_KEY );
        }

        /**
         * Retrieve the Merchant ID.
         *
         * @since 1.0.0
         * @return string
         */
        public static function getMerchantID()
        {
            return get_option( WC_SPLASH_PAYMENTS_SETTINGS_MERCHANT_ID );
        }

        /**
         * Return true if we are in testing mode, false otherwise.
         *
         * @since 1.0.0
         * @return boolean
         */
        public static function getTestingStatus()
        {
            $testing_status = get_option( WC_SPLASH_PAYMENTS_SETTINGS_TESTING );
            return $testing_status === 'on' || $testing_status === 'yes';
        }

        /**
         * Get Splash Payments amount to pay.
         *
         * @param float $total
         *
         * @return float|int
         */
        public static function get_splash_amount( $total )
        {
            return absint( wc_format_decimal( ( (float)$total * 100 ), wc_get_price_decimals() ) );
        }

        /**
         * Checks for a value in an array recursively. Works for multi-dimensional arrays.
         *
         * @since 1.0.0
         * @param mixed $needle
         * @param array $haystack
         * @param boolean $strict
         * @return boolean
         */
        public static function in_array_r( $needle, $haystack, $strict = false )
        {
            foreach( $haystack as $item )
            {
                if( ( $strict ? $item === $needle : $item == $needle ) || ( is_array( $item ) && WC_Splash_Payments_Utils::in_array_r( $needle, $item, $strict ) ) )
                {
                    return true;
                }
            }

            return false;
        }
    }

endif;
