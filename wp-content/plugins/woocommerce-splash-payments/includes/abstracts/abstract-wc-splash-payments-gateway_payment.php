<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) )
{
    exit;
}

if( !class_exists( 'WC_Splash_Payments_Gateway_Payment' ) ):

    /**
     * Abstract class that will be inherited by all payment methods.
     *
     * @extends WC_Payment_Gateway_CC
     *
     * @since 4.0.0
     */
    abstract class WC_Splash_Payments_Gateway_Payment extends WC_Payment_Gateway_CC
    {
        /**
         * Is $order_id a subscription?
         * @param  int $order_id
         * @return boolean
         */
        public function has_subscription( $order_id )
        {
            return ( function_exists( 'wcs_order_contains_subscription' )
                     && ( wcs_order_contains_subscription( $order_id )
                          || wcs_is_subscription( $order_id )
                          || wcs_order_contains_renewal( $order_id ) ) );
        }

        /**
         * Does this customer exist?
         * @param  string $customer_id
         * @return boolean
         */
        public function has_customer( $customer_id )
        {
            $data = array(
                'id' => $customer_id
            );

            WC_Splash_Payments_Utils::setAPIPrivate();
            $customer = new \SplashPayments\customers( $data );

            try
            {
                $customer->retrieve();

                return $customer->getResponse();
            }
            catch( \SplashPayments\Exceptions\Base $e )
            {
                return false;
            }
        }

        /**
         * Returns token secures string if found.
         * @param  string $token_id
         * @return string
         */
        public function get_token( $token_id )
        {
            $data = array(
                'id' => $token_id,
            );

            WC_Splash_Payments_Utils::setAPIPrivate();
            $token = new \SplashPayments\tokens( $data );

            try
            {
                $token->retrieve();

                if( !$token->hasErrors() )
                {
                    return $token->getResponse()[ 0 ]->token;
                }
                else
                {
                    return '';
                }
            }
            catch( \SplashPayments\Exceptions\Base $e )
            {
                // Handle exceptions
                return '';
            }
        }

        /**
         * Tries to create the customer for Splash Payments.
         *
         * @param WC_Order $order
         */
        public function maybe_create_customer( $order )
        {
            $user               = get_current_user_id();
            $splash_customer_id = get_user_meta( $user, '_splash_customer_id', true );
            $create_customer    = !$this->has_customer( $splash_customer_id );

            //Create customer, possibly
            if( $create_customer )
            {
                $data = array(
                    'merchant' => WC_Splash_Payments_Utils::getMerchantID(),
                    'first'    => $order->get_billing_first_name(),
                    'last'     => $order->get_billing_last_name(),
                    'email'    => $order->get_billing_email(),
                    'company'  => $order->get_billing_company(),
                    'address1' => $order->get_billing_address_1(),
                    'address2' => $order->get_billing_address_2(),
                    'city'     => $order->get_billing_city(),
                    'state'    => $order->get_billing_state(),
                    'zip'      => $order->get_billing_postcode(),
                    'country'  => $order->get_billing_country(),
                    'phone'    => $order->get_billing_phone()
                );

                WC_Splash_Payments_Utils::setAPIPublic();
                $new_customer = new \SplashPayments\customers( $data );

                try
                {
                    $new_customer->create();

                    if( !$new_customer->hasErrors() )
                    {
                        update_user_meta( $user, '_splash_customer_id', $new_customer->getResponse()[ 0 ]->id );
                        $order->add_order_note( sprintf( __( 'Customer created on Splash Payments: %s', 'wc_splash_payments' ), $new_customer->getResponse()[ 0 ]->id ) );
                    }
                }
                catch( \SplashPayments\Exceptions\Base $e )
                {
                    // Handle exceptions
                    $order->add_order_note( sprintf( __( 'Error creating customer: %s', 'wc_splash_payments' ), $new_customer->getErrors()[0]['msg'] ) );
                }
            }
            else
            {
                $order->add_order_note( sprintf( __( 'Customer already exists on Splash Payments: %s', 'wc_splash_payments' ), $splash_customer_id ) );
            }
        }

        /**
         * Tries to a token for a customer for Splash Payments.
         *
         * @param WC_Order $order
         * @param array $data
         */
        public function maybe_create_token( $order, $data )
        {
            $user               = get_current_user_id();
            $splash_customer_id = get_user_meta( $user, '_splash_customer_id', true );
            $token_id           = get_user_meta( $user, '_splash_customer_token_id', true );
            $create_token       = $this->has_customer( $splash_customer_id ) && empty( $token_id );

            //Create token, possibly
            if( $create_token )
            {
                //Setup credit card detector
                $detector = new CardDetect\Detector();
                $card     = $data[ 'payment' ][ 'number' ];
                $type     = $detector->detect( $card );

                //Credit Card
                if( $type == 'Amex' )
                {
                    $method = 1;
                }
                elseif( $type == 'Visa' )
                {
                    $method = 2;
                }
                elseif( $type == 'MasterCard' )
                {
                    $method = 3;
                }
                elseif( $type == 'DinersClub' )
                {
                    $method = 4;
                }
                elseif( $type == 'Discover' )
                {
                    $method = 5;
                }
                else //eCheck
                {
                    $method = 8;
                }

                //Credit Card
                if( $method == 8 || $method == 9 || $method == 10 || $method == 11 )
                {
                    $token_data = array(
                        'customer'   => $splash_customer_id,
                        'payment'    => array(
                            'method' => $method,
                            'number' => $data[ 'payment' ][ 'number' ]
                        ),
                        'expiration' => $data[ 'expiration' ]
                    );
                }
                else //eCheck
                {
                    $token_data = array(
                        'customer' => $splash_customer_id,
                        'payment'  => array(
                            'method'  => $method,
                            'number'  => $data[ 'payment' ][ 'number' ],
                            'routing' => $data[ 'payment' ][ 'routing' ]
                        )
                    );
                }

                WC_Splash_Payments_Utils::setAPIPublic();
                $new_token = new \SplashPayments\tokens( $token_data );

                try
                {
                    $new_token->create();

                    if( !$new_token->hasErrors() )
                    {
                        update_user_meta( $user, '_splash_customer_token_id', $new_token->getResponse()[ 0 ]->id );
                        $order->add_order_note( sprintf( __( 'Token created on Splash Payments: %s', 'wc_splash_payments' ), print_r( $new_token->getResponse()[ 0 ] ) ) );
                    }
                }
                catch( \SplashPayments\Exceptions\Base $e )
                {
                    // Handle exceptions
                    $order->add_order_note( sprintf( __( 'Error creating token: %s', 'wc_splash_payments' ), $new_token->getErrors()[0]['msg'] ) );
                }
            }
            else
            {
                $order->add_order_note( sprintf( __( 'Token already exists on Splash Payments: %s', 'wc_splash_payments' ), $token_id ) );
            }
        }

        /**
         * Processes the payment response.
         *
         * @param array $response
         * @param WC_Order $order
         * @return array
         */
        public function process_response( $response, $order )
        {


            return $response;
        }

        /**
         * Processes the payment through Splash Payments.
         *
         * @param int $order_id
         * @param array $data
         * @return array
         */
        public function handle_payment( $order_id, $data )
        {
            //Get current order
            $order = wc_get_order( $order_id );

            try
            {
                //NOT a Subscription
                if( !$this->has_subscription( $order_id ) )
                {
                    //Create regular transaction
                    WC_Splash_Payments_Utils::setAPIPublic();
                    $transaction = new \SplashPayments\txns( $data );

                    try
                    {
                        $transaction->create();

                        if( $transaction->hasErrors() )
                        {
                            $error_message = __( 'Splash charge failed.', 'wc_splash_payments' );
                            throw new Exception( $error_message );
                        }

                        return $this->handle_payment_success( $order, $transaction, $data );
                    }
                    catch( \SplashPayments\Exceptions\Base $e )
                    {
                        $errors     = $transaction->getErrors()[ 0 ];
                        $error_data = var_export( $errors, true );
                        $order->add_order_note( $error_data );
                        return $this->handle_payment_error(
                            $order,
                            'Splash Payments - Transaction Error: ',
                            $transaction->getErrors()[ 0 ][ 'msg' ]
                        );
                    }
                } //IS a Subscription
                else
                {
                    /*
                    $sign_up_fee               = WC_Subscriptions_Order::get_sign_up_fee( $order );
                    $initial_payment           = WC_Subscriptions_Order::get_total_initial_payment( $order );
                    $subscription_trial_period     = WC_Subscriptions_Order::get_subscription_trial_period( $order );
                    $subscription_trial_length     = WC_Subscriptions_Order::get_subscription_trial_length( $order );
                    */

                    $subscription_price_per_period = WC_Subscriptions_Order::get_recurring_total( $order );
                    $subscription_schedule         = WC_Subscriptions_Order::get_subscription_period( $order );
                    $subscription_schedule_factor  = WC_Subscriptions_Order::get_subscription_interval( $order );
                    $subscription_start_date       = date( 'Ymd' );

                    //Get the NEXT payment date, AFTER free trial
                    foreach( $order->get_items() as $item )
                    {
                        $date                    = WC_Subscriptions_Order::get_next_payment_date( $order, $item[ 'product_id' ] ); /* This should get the next payment date... */
                        $subscription_start_date = date( 'Ymd', strtotime( $date ) );
                        break;
                    }

                    if( $subscription_schedule == 'day' ) //Day
                    {
                        $subscription_schedule = 1;
                    }
                    else if( $subscription_schedule == 'week' ) //Week
                    {
                        $subscription_schedule = 2;
                    }
                    else if( $subscription_schedule == 'month' ) //Month
                    {
                        $subscription_schedule = 3;
                    }
                    else //Year
                    {
                        $subscription_schedule = 4;
                    }

                    $plan_data = array(
                        'merchant'       => WC_Splash_Payments_Utils::getMerchantID(),
                        'schedule'       => $subscription_schedule, //1 = Daily, 2 = Weekly, 3 = Monthly, 4 = Yearly
                        'scheduleFactor' => $subscription_schedule_factor,
                        'amount'         => $subscription_price_per_period
                    );

                    //Create subscription plan
                    WC_Splash_Payments_Utils::setAPIPrivate();
                    $plan = new \SplashPayments\plans( $plan_data );

                    try
                    {
                        $plan->create();

                        WC_Splash_Payments_Utils::setAPIPrivate();
                        $subscription = new \SplashPayments\subscriptions(
                            array(
                                'plan'  => $plan->getResponse()[ 0 ]->id,
                                'start' => $subscription_start_date
                            )
                        );

                        try
                        {
                            $subscription->create();

                            //Maybe create a customer
                            $this->maybe_create_customer( $order );

                            //Maybe create a token
                            $this->maybe_create_token( $order, $data );

                            WC_Splash_Payments_Utils::setAPIPrivate();
                            $token = $this->get_token( get_user_meta( get_current_user_id(), '_splash_customer_token_id', true ) );
                            if( empty( $token ) )
                            {
                                throw new Exception( 'No token was found.' );
                            }

                            $subscription_token = new \SplashPayments\subscriptionTokens(
                                array(
                                    'subscription' => $subscription->getResponse()[ 0 ]->id,
                                    'token'        => $token
                                )
                            );

                            try
                            {
                                $subscription_token->create();

                                if( !$subscription_token->hasErrors() )
                                {
                                    //Success!
                                    // Handle exceptions
                                    $order->add_order_note( __( 'Splash Payments - Subscription Success!', 'wc_splash_payments' ) );

                                    return $this->handle_payment_success( $order, $subscription, $data );
                                }
                            }
                            catch( \SplashPayments\Exceptions\Base $e )
                            {
                                return $this->handle_payment_error(
                                    $order,
                                    'Splash Payments - Subscription Token Error: ',
                                    $subscription_token->getErrors()[ 0 ][ 'msg' ]
                                );
                            }
                        }
                        catch( \SplashPayments\Exceptions\Base $e )
                        {
                            return $this->handle_payment_error(
                                $order,
                                'Splash Payments - Subscription Error: ',
                                $subscription->getErrors()[ 0 ][ 'msg' ]
                            );
                        }
                    }
                    catch( \SplashPayments\Exceptions\Base $e )
                    {
                        return $this->handle_payment_error(
                            $order,
                            'Splash Payments - Plan Error: ',
                            $plan->getErrors()[ 0 ][ 'msg' ]
                        );
                    }
                }
            }
            catch( Exception $e )
            {
                return $this->handle_payment_error(
                    $order,
                    'Splash Payments - Checkout Error: ',
                    $e->getMessage()
                );
            }

            // Return thank you redirect
            return array(
                'result'   => 'success',
                'redirect' => $this->get_return_url( $order )
            );
        }

        /**
         * Processes a refund through Splash Payments.
         *
         * @param int $order_id
         * @param float $amount
         * @param string $reason
         * @param array $data
         * @return boolean
         */
        // public function handle_refund( $order_id, $amount, $reason, $data )
        // {
        //     $order = wc_get_order( $order_id );

        //     WC_Splash_Payments_Utils::setAPIPrivate();
        //     $refund_transaction = new \SplashPayments\txns( $data );

        //     try
        //     {
        //         $refund_transaction->create();

        //         if( empty( $refund_transaction->hasErrors() ) )
        //         {
        //             $refund_message = sprintf( __( 'Refunded %1$s - Refund ID: %2$s - Reason: %3$s', 'wc_splash_payments' ), wc_price( $amount ), $order->get_transaction_id(), $reason );
        //             $order->add_order_note( $refund_message );

        //             return true;
        //         }
        //         else
        //         {
        //             return false;
        //         }
        //     }
        //     catch( \SplashPayments\Exceptions\Base $e )
        //     {
        //         $order->add_order_note( __( 'Refund Failed: [msg]: ' . $refund_transaction->getErrors()[ 0 ][ 'msg' ] . ', [field]: ' . $refund_transaction->getErrors()[0]['field'] . ' - ' . $order->get_transaction_id(), 'wc_splash_payments' ) );
        //         return false;
        //     }
        // }




















        /**
         * Processes a refund through Splash Payments.
         *
         * @param WC_Order $order
         * @param string $transaction_id
         * @param string $reason
         * @param Array $data
         * @return boolean
         */
        public function handle_refund( $order, $transaction_id, $reason, $data )
        {
            // $order = wc_get_order( $order_id );

            // WC_Splash_Payments_Utils::setAPIPrivate();
            // $refund_transaction = new \SplashPayments\txns( $data );

            // try
            // {
            //     $refund_transaction->create();

            //     if( empty( $refund_transaction->hasErrors() ) )
            //     {
            //         $refund_message = sprintf( __( 'Refunded %1$s - Refund ID: %2$s - Reason: %3$s', 'wc_splash_payments' ), wc_price( $amount ), $order->get_transaction_id(), $reason );
            //         $order->add_order_note( $refund_message );

            //         return true;
            //     }
            //     else
            //     {
            //         return false;
            //     }
            // }
            // catch( \SplashPayments\Exceptions\Base $e )
            // {
            //     $order->add_order_note( __( 'Refund Failed: [msg]: ' . $refund_transaction->getErrors()[ 0 ][ 'msg' ] . ', [field]: ' . $refund_transaction->getErrors()[0]['field'] . ' - ' . $order->get_transaction_id(), 'wc_splash_payments' ) );
            //     return false;
            // }

            WC_Splash_Payments_Utils::setAPIPrivate();
            $refund_transaction = new \SplashPayments\txns( $data );

            try
            {
                $refund_transaction->create();

                if( empty( $refund_transaction->hasErrors() ) )
                {
                    $refund_message = sprintf( __( 'Refunded %1$s - Refund ID: %2$s - Reason: %3$s', 'wc_splash_payments' ), wc_price( $amount ), $transaction_id, $reason );
                    $order->add_order_note( $refund_message );

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch( \SplashPayments\Exceptions\Base $e )
            {
                $order->add_order_note( __( 'Refund Failed: [msg]: ' . $refund_transaction->getErrors()[ 0 ][ 'msg' ] . ', [field]: ' . $refund_transaction->getErrors()[0]['field'] . ' - ' . $order->get_transaction_id(), 'wc_splash_payments' ) );
                return false;
            }
        }

















        /**
         * Moves forward when a payment is successfully processed.
         *
         * @param WC_Order $order
         * @param mixed $transaction
         * @param array $data
         * @return array
         */
        private function handle_payment_success( $order, $transaction, $data )
        {
            //Get order id
            $order_id = $order->get_id();

            //Update WooCommerce transaction id to match the Splash transactions ID
            update_post_meta( $order_id, '_transaction_id', $transaction->getResponse()[ 0 ]->id );

            //Payment complete
            $order->payment_complete( $transaction->getResponse()[ 0 ]->id );

            //Add order note
            $complete_message = sprintf( __( 'Splash charge complete (Charge ID: %s).', 'wc_splash_payments' ), $transaction->getResponse()[ 0 ]->id );
            $order->add_order_note( $complete_message );

            //Maybe create a customer
            //$this->maybe_create_customer( $order );

            //Maybe create a token
            //$this->maybe_create_token( $order, $data );

            //Remove cart
            WC()->cart->empty_cart();

            //Return thank you redirect
            return array(
                'result'   => 'success',
                'redirect' => $this->get_return_url( $order )
            );
        }

        /**
         * Processes an error if one occurred during the payment processing.
         *
         * @param WC_Order $order
         * @param string $error_msg
         * @param string $notice_msg
         * @return array
         */
        private function handle_payment_error( $order, $notice_msg, $error_msg )
        {
            //Add note to order, notifying admin that this order didn't go through
            $order->add_order_note( __( '<b>' . $notice_msg . '</b>' . $error_msg, 'wc_splash_payments' ) );
            $order->update_status( 'failed' );

            //Add notice to front end, notifying user that the order failed
            wc_add_notice( __( $notice_msg . $error_msg, 'wc_splash_payments' ), 'error' );

            //Return as a failure, so the order doesn't go through
            return array(
                'result' => 'failure'
            );
        }
    }

endif;
