<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) )
{
    exit;
}

if( !class_exists( 'WC_Splash_Payments_Gateway_eCheck' ) ):

    class WC_Splash_Payments_Gateway_eCheck extends WC_Splash_Payments_Gateway_Payment
    {
        /**
         * Constructor for the gateway.
         */
        public function __construct()
        {
            $this->id                 = 'splash_payments_echeck';
            $this->method_title       = __( 'Splash Payments (eCheck)', 'wc_splash_payments' );
            $this->method_description = __( 'Allows the use of bank accounts through Splash Payments.', 'wc_splash_payments' );
            $this->supports           = array(
                'products',
                'refunds',
                'subscriptions'
            );

            // Load the settings.
            $this->init_form_fields();
            $this->init_settings();

            // Define user set variables
            $this->title       = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );

            // Actions
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'update_settings' ) );
            add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );

            // Customer Emails
            add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
        }

        /**
         * Initialize Gateway Settings Form Fields
         */
        public function init_form_fields()
        {

            $this->form_fields = apply_filters( 'wc_splash_payments_form_fields', array(

                'enabled' => array(
                    'title'   => __( 'Enable/Disable', 'wc_splash_payments' ),
                    'type'    => 'checkbox',
                    'label'   => __( 'Enable eCheck', 'wc_splash_payments' ),
                    'default' => 'yes'
                ),

                'title' => array(
                    'title'       => __( 'Title', 'wc_splash_payments' ),
                    'type'        => 'text',
                    'description' => __( 'This controls the title for the payment method the customer sees during checkout.', 'wc_splash_payments' ),
                    'default'     => __( 'eCheck', 'wc_splash_payments' ),
                    'desc_tip'    => true,
                ),

                'description' => array(
                    'title'       => __( 'Description', 'wc_splash_payments' ),
                    'type'        => 'textarea',
                    'description' => __( 'Payment method description that the customer will see on your checkout.', 'wc_splash_payments' ),
                    'default'     => __( 'Pay with your bank account via Splash Payments.', 'wc_splash_payments' ),
                    'desc_tip'    => true,
                ),
            ) );
        }

        /**
         * Output for the order received page.
         */
        public function thankyou_page()
        {
            if( $this->instructions )
            {
                echo wpautop( wptexturize( $this->instructions ) );
            }
        }

        /**
         * Add content to the WC emails.
         *
         * @access public
         * @param WC_Order $order
         * @param bool $sent_to_admin
         * @param bool $plain_text
         */
        public function email_instructions( $order, $sent_to_admin, $plain_text = false )
        {

            if( $this->instructions && !$sent_to_admin && $this->id === $order->payment_method && $order->has_status( 'on-hold' ) )
            {
                echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
            }
        }

        public function payment_fields()
        {
            ?>

            <fieldset style="padding: 0 !important;">
                <?php
                $allowed = array(
                    'a'      => array(
                        'href'  => array(),
                        'title' => array(),
                    ),
                    'br'     => array(),
                    'em'     => array(),
                    'strong' => array(),
                    'span'   => array(
                        'class' => array(),
                    ),
                );

                if( $this->description )
                {
                    echo wpautop( wp_kses( $this->description, $allowed ) );
                }
                ?>
                <p class="form-row form-row-wide">
                    <label for="splash-account-number"><?php esc_html_e( 'Account Number', 'wc_splash_payments' ); ?>
                        <span
                                class="required">*</span></label>
                    <input id="splash-account-number" type="text" maxlength="20" autocomplete="off"
                           placeholder="••••••" name="<?php echo esc_attr( $this->id ); ?>-account-number"/>
                </p>

                <p class="form-row form-row-wide">
                    <label for="splash-routing-number"><?php esc_html_e( 'Routing Number', 'wc_splash_payments' ); ?>
                        <span
                                class="required">*</span></label>
                    <input id="splash-routing-number" type="text" maxlength="20" autocomplete="off"
                           placeholder="••••••••••" name="<?php echo esc_attr( $this->id ); ?>-routing-number"/>
                </p>
            </fieldset>

            <?php
        }

        /**
         * Process the payment and return the result
         *
         * @param int $order_id
         * @return array
         */
        public function process_payment( $order_id )
        {
            $order = wc_get_order( $order_id );

            $account_number = '';
            $routing_number = '';

            //Get account number
            if( isset( $_POST[ $this->id . '-account-number' ] ) )
            {
                $account_number = $_POST[ $this->id . '-account-number' ];
            }

            //Get routing number
            if( isset( $_POST[ $this->id . '-routing-number' ] ) )
            {
                $routing_number = $_POST[ $this->id . '-routing-number' ];
            }

            //Make sure card number was set
            if( empty( $account_number ) )
            {
                wc_add_notice( __( 'Account Number not set.', 'wc_splash_payments' ), 'error' );
            }

            //Make sure card expiration was set
            if( empty( $routing_number ) )
            {
                wc_add_notice( __( 'Routing Number not set.', 'wc_splash_payments' ), 'error' );
            }

            $data = array(
                'merchant' => WC_Splash_Payments_Utils::getMerchantID(),
                'type'     => 7,
                'origin'   => 2,
                'payment'  => array(
                    'method'  => 8,
                    'number'  => $account_number,
                    'routing' => $routing_number
                ),
                'total'    => (int)$order->get_total(),
                'first'    => $order->get_billing_first_name(),
                'last'     => $order->get_billing_last_name(),
                'email'    => $order->get_billing_email(),
                'company'  => $order->get_billing_company(),
                'address1' => $order->get_billing_address_1(),
                'address2' => $order->get_billing_address_2(),
                'zip'      => $order->get_billing_postcode(),
                'state'    => $order->get_billing_state(),
                'city'     => $order->get_shipping_city()
            );

            return parent::handle_payment( $order_id, $data );
        }

        /**
         * Refund a transaction.
         * @param int $order_id
         * @param float $amount
         * @param string $reason
         * @return bool
         */
        public function process_refund( $order_id, $amount = null, $reason = '' )
        {
            $order          = wc_get_order( $order_id );
            $transaction_id = $order->get_transaction_id();
            $data           = array(
                'type'   => 8,
                'first'  => wp_get_current_user()->first_name,
                'last'   => wp_get_current_user()->last_name,
                'fortxn' => $transaction_id,
                'total'  => $amount
            );

            return $this->handle_refund( $order_id, $amount, $reason, $data );
        }
    }

endif;
