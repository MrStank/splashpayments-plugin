<?php
/**
 * Splash Payments Payment Request API
 *
 * @since 1.0.0
 */

if( !defined( 'ABSPATH' ) )
{
    exit;
}

if( !class_exists( 'WC_Splash_Payments_Payment_Request' ) ):

    /**
     * WC_Splash_Payments_Payment_Request class.
     */
    class WC_Splash_Payments_Payment_Request
    {
        /**
         * This Instance.
         *
         * @var WC_Splash_Payments_Payment_Request
         */
        private static $_this;

        /**
         * Initialize class actions.
         *
         * @since 1.0.0
         * @version 1.0.0
         */
        public function __construct()
        {
            self::$_this = $this;

            // Don't load for change payment method page.
            if( isset( $_GET[ 'change_payment_method' ] ) )
            {
                return;
            }

            add_action( 'template_redirect', array( $this, 'set_session' ) );

            $this->init();
        }

        /**
         * Sets the WC customer session if one is not set.
         * This is needed so nonces can be verified by AJAX Request.
         *
         * @since 1.0.0
         */
        public function set_session()
        {
            if( !is_product() || ( isset( WC()->session ) && WC()->session->has_session() ) )
            {
                return;
            }

            $session_class = apply_filters( 'woocommerce_session_handler', 'WC_Session_Handler' );
            $wc_session    = new $session_class();

            if( version_compare( WC_VERSION, '3.3', '>=' ) )
            {
                $wc_session->init();
            }

            $wc_session->set_customer_session_cookie( true );
        }

        /**
         * Initialize hooks.
         *
         * @since 1.0.0
         * @version 1.0.0
         */
        public function init()
        {
            add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );

            // add_action( 'wc_ajax_wc_splash_get_cart_details', array( $this, 'ajax_get_cart_details' ) );
            // add_action( 'wc_ajax_wc_splash_payment_success', array( $this, 'ajax_payment_success' ) );
        }

        /**
         * Load public scripts and styles.
         *
         * @since 1.0.0
         * @version 1.0.0
         */
        public function scripts()
        {
            // If keys are not set bail.
            if( !$this->are_keys_set() )
            {
                WC_Splash_Payments_Logger::log( 'Keys are not set correctly.' );
                return;
            }

            // If no SSL bail.
            if( !WC_Splash_Payments_Utils::getTestingStatus() && !is_ssl() )
            {
                WC_Splash_Payments_Logger::log( 'Splash Payments requires SSL.' );
                return;
            }

            if( !is_product() && !is_cart() && !is_checkout() && !isset( $_GET[ 'pay_for_order' ] ) )
            {
                return;
            }

            if( is_product() )
            {
                global $post;

                $product = wc_get_product( $post->ID );

                if( !is_object( $product ) || !in_array( ( $product->get_type() ), $this->supported_product_types() ) )
                {
                    return;
                }

                if( apply_filters( 'wc_splash_hide_payment_request_on_product_page', false, $post ) )
                {
                    return;
                }
            }
            
            //Load checkout-only scripts
            if(is_checkout() && !is_order_received_page())
            {
                wp_register_script( 'wc_splash_payments_request', plugins_url( '/assets/js/splash_checkout.js', WC_SPLASH_MAIN_FILE ), array( 'jquery' ), WC_SPLASH_PAYMENTS_VERSION, true );
                wp_localize_script(
                    'wc_splash_payments_request',
                    'wc_splash_payments_request_params',
                    array(
                        'ajax_url'         => WC_AJAX::get_endpoint( '%%endpoint%%' ),
                        'api_key'          => WC_Splash_Payments_Utils::getPublicAPIKey(),
                        'merchant_id'      => WC_Splash_Payments_Utils::getMerchantID(),
                        // 'is_product_page'  => is_product(),
                        'order'            => $this->get_order_data(),
                        // 'product'          => $this->get_product_data(),
                        'isTesting'        => WC_Splash_Payments_Utils::getTestingStatus(),
                        'paymentMethods'   => array( 'CC' => 'splash_payments_cc', 'eCheck' => 'splash_payments_echeck' )
                    )
                );
                wp_enqueue_script( 'wc_splash_payments_request' );
            }
        }

        public function ajax_payment_success()
        {
            //global $woocommerce, $post;

            //$order = wc_get_order($post->ID);

            //$request = $_REQUEST['request'];
            //$order->add_order_note('Test');

            //$data = array(
                //'msg' => 'My Message'
            //);
            //wp_send_json( $data );

            /*
            //Get order id
            $order_id = $order->get_id();

            //Update WooCommerce transaction id to match the Splash transactions ID
            update_post_meta( $order_id, '_transaction_id', $transaction->getResponse()[ 0 ]->id );

            //Payment complete
            $order->payment_complete( $transaction->getResponse()[ 0 ]->id );

            //Add order note
            $complete_message = sprintf( __( 'Splash charge complete (Charge ID: %s).', 'wc_splash_payments' ), $transaction->getResponse()[ 0 ]->id );
            $order->add_order_note( $complete_message );

            //Maybe create a customer
            $this->maybe_create_customer( $order );

            //Maybe create a token
            $this->maybe_create_token( $order, $data );

            //Remove cart
            WC()->cart->empty_cart();

            //Return thank you redirect
            return array(
                'result'   => 'success',
                'redirect' => $this->get_return_url( $order )
            );
             */

            /*
            global $woocommerce, $post;

            $order = new WC_Order($post->ID);

            $msg = 'test';
            $data = array(
                'msg' => 'my msg',
                //'response' => $response
            );

            wp_send_json( $data );

            //Get order id
            $order_id = $order->get_id();

            //Update WooCommerce transaction id to match the Splash transactions ID
            $response = $_REQUEST['response'];
            update_user_meta(get_current_user_id(), '_transaction_id', $response->data[0]->id);

            $order->add_order_note('On no');
            $order->set_status('on-hold');
            */

            exit;
        }

        /**
         * Checks to make sure product type is supported.
         *
         * @since 1.0.0
         * @version 1.0.0
         * @return array
         */
        public function supported_product_types()
        {
            return apply_filters( 'wc_splash_payment_request_supported_types', array(
                'simple',
                'variable',
                'variation',
                'subscription',
            ) );
        }

        /**
         * Checks if keys are set.
         *
         * @since 1.0.0
         * @return bool
         */
        public function are_keys_set()
        {
            if( empty( WC_Splash_Payments_Utils::getPublicAPIKey() ) || empty( WC_Splash_Payments_Utils::getMerchantID() ) )
            {
                return false;
            }

            return true;
        }

        /**
         * Gets the order data for the checkout.
         *
         * @since 1.0.0
         * @version 1.0.0
         */
        public function get_order_data()
        {
            if( !is_checkout() )
            {
                return false;
            }

            global $woocommerce;

            $data            = array();
            $data[ 'total' ] = array(
                'amount' => WC_Splash_Payments_Utils::get_splash_amount( WC()->cart->total )
            );

            return $data;
        }
    }

    new WC_Splash_Payments_Payment_Request;

endif;
