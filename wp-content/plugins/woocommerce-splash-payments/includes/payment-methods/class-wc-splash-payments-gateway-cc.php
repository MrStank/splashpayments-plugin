<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) )
{
    exit;
}

if( !class_exists( 'WC_Splash_Payments_Gateway_CC' ) ):

    class WC_Splash_Payments_Gateway_CC extends WC_Splash_Payments_Gateway_Payment
    {
        /**
         * Constructor for the gateway.
         */
        public function __construct()
        {
            $this->id                 = 'splash_payments_cc';
            $this->method_title       = __( 'Splash Payments', 'wc_splash_payments' );
            $this->method_description = __( 'Allows the use of a credit card through Splash Payments.', 'wc_splash_payments' );
            $this->supports           = array(
                'products',
                'refunds',
                'subscriptions'
            );

            // Load the settings.
            $this->init_form_fields();
            $this->init_settings();

            // Define user set variables
            $this->title        = $this->get_option( 'title' );
            $this->description  = $this->get_option( 'description' );

            // Actions
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'update_settings' ) );
            add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );

            // Customer Emails
            add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
        }

        /**
         * Initialize Gateway Settings Form Fields
         */
        public function init_form_fields()
        {
            $this->form_fields = apply_filters( 'wc_splash_payments_form_fields', array(

                'enabled' => array(
                    'title'   => __( 'Enable/Disable', 'wc_splash_payments' ),
                    'type'    => 'checkbox',
                    'label'   => __( 'Enable Credit Card', 'wc_splash_payments' ),
                    'default' => 'yes'
                ),

                'title' => array(
                    'title'       => __( 'Title', 'wc_splash_payments' ),
                    'type'        => 'text',
                    'description' => __( 'This controls the title for the payment method the customer sees during checkout.', 'wc_splash_payments' ),
                    'default'     => __( 'Credit Card (Splash Payments)', 'wc_splash_payments' ),
                    'desc_tip'    => true,
                ),

                'description' => array(
                    'title'       => __( 'Description', 'wc_splash_payments' ),
                    'type'        => 'textarea',
                    'description' => __( 'Payment method description that the customer will see on your checkout.', 'wc_splash_payments' ),
                    'default'     => __( 'Pay with your credit card via Splash Payments.', 'wc_splash_payments' ),
                    'desc_tip'    => true,
                ),

                'private_api_key' => array(
                    'title'       => __( 'Private API Key', 'wc_splash_payments' ),
                    'type'        => 'password',
                    'description' => __( 'Private Splash Payments API Key.', 'wc_splash_payments' ),
                    'default'     => __( WC_Splash_Payments_Utils::getPrivateAPIKey(), 'wc_splash_payments' ),
                    'desc_tip'    => true,
                ),

                'public_api_key' => array(
                    'title'       => __( 'Public API Key', 'wc_splash_payments' ),
                    'type'        => 'password',
                    'description' => __( 'Public Splash Payments API Key.', 'wc_splash_payments' ),
                    'default'     => __( WC_Splash_Payments_Utils::getPublicAPIKey(), 'wc_splash_payments' ),
                    'desc_tip'    => true,
                ),

                'merchant_id' => array(
                    'title'       => __( 'Merchant ID', 'wc_splash_payments' ),
                    'type'        => 'password',
                    'description' => __( 'Splash Payments Merchent ID.', 'wc_splash_payments' ),
                    'default'     => __( WC_Splash_Payments_Utils::getMerchantID(), 'wc_splash_payments' ),
                    'desc_tip'    => true,
                ),

                'testing' => array(
                    'title'       => __( 'Testing', 'wc_splash_payments' ),
                    'type'        => 'checkbox',
                    'description' => __( 'Check this box to enable testing mode.', 'wc_splash_payments' ),
                    'default'     => WC_Splash_Payments_Utils::getTestingStatus(),
                    'desc_tip'    => true,
                ),
            ) );
        }

        public function update_settings()
        {
            $this->process_admin_options();

            update_option( WC_SPLASH_PAYMENTS_SETTINGS_PRIVATE_API_KEY, $this->get_option( 'private_api_key' ) );
            update_option( WC_SPLASH_PAYMENTS_SETTINGS_PUBLIC_API_KEY, $this->get_option( 'public_api_key' ) );
            update_option( WC_SPLASH_PAYMENTS_SETTINGS_MERCHANT_ID, $this->get_option( 'merchant_id' ) );
            update_option( WC_SPLASH_PAYMENTS_SETTINGS_TESTING, $this->get_option( 'testing' ) );
        }

        /**
         * Output for the order received page.
         */
        public function thankyou_page()
        {
            if( $this->instructions )
            {
                echo wpautop( wptexturize( $this->instructions ) );
            }
        }

        /**
         * Add content to the WC emails.
         *
         * @access public
         * @param WC_Order $order
         * @param bool $sent_to_admin
         * @param bool $plain_text
         */
        public function email_instructions( $order, $sent_to_admin, $plain_text = false )
        {

            if( $this->instructions && !$sent_to_admin && $this->id === $order->payment_method && $order->has_status( 'on-hold' ) )
            {
                echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
            }
        }

        public function payment_fields()
        {
            ?>

            <fieldset>
                <?php
                $allowed = array(
                    'a'      => array(
                        'href'  => array(),
                        'title' => array(),
                    ),
                    'br'     => array(),
                    'em'     => array(),
                    'strong' => array(),
                    'span'   => array(
                        'class' => array(),
                    ),
                );

                if( $this->description )
                {
                    echo wpautop( wp_kses( $this->description, $allowed ) );
                }
                ?>
                <div>
                    <label for="number">Number:</label>
                    <div id="number"></div>
                </div>

                <div>
                    <label for="cvv">CVV:</label>
                    <div id="cvv"></div>
                </div>

                <div>
                    <label for="name">Name:</label>
                    <div id="name"></div>
                </div>

                <div>
                    <label for="address">Address:</label>
                    <div id="address"></div>
                </div>

                <div>
                    <label for="expiration">Expiration:</label>
                    <div id="expiration"></div>
                </div>

                <input type="hidden" id="_transID" name="_transID">
            </fieldset>

            <?php
        }

        /**
         * Process the payment and return the result
         *
         * @param int $order_id
         * @return array
         */
        /*
        public function process_payment( $order_id )
        {
            $order = wc_get_order( $order_id );

            $card_number     = '';
            $card_expiration = '';
            $card_cvv        = '';

            //Get card number
            if( isset( $_POST[ $this->id . '-card-number' ] ) )
            {
                $card_number = $_POST[ $this->id . '-card-number' ];
            }

            //Get card expiration date
            if( isset( $_POST[ $this->id . '-card-expiry' ] ) )
            {
                $card_expiration = $_POST[ $this->id . '-card-expiry' ];
            }

            //Get card CCV number
            if( isset( $_POST[ $this->id . '-card-cvv' ] ) )
            {
                $card_cvv = $_POST[ $this->id . '-card-cvv' ];
            }

            //Make sure card number was set
            if( empty( $card_number ) )
            {
                wc_add_notice( __( 'Card Number not set.', 'wc_splash_payments' ), 'error' );
                return array(
                    'result' => 'error',
                );
            }

            //Make sure card expiration was set
            if( empty( $card_expiration ) )
            {
                wc_add_notice( __( 'Card Expiration not set.', 'wc_splash_payments' ), 'error' );
                return array(
                    'result' => 'error',
                );
            }

            //Make sure card CVV was set
            if( empty( $card_cvv ) )
            {
                wc_add_notice( __( 'Card CVV not set.', 'wc_splash_payments' ), 'error' );

                return array(
                    'result' => 'error',
                );
            }

            $data = array(
                'merchant'   => WC_Splash_Payments_Utils::getMerchantID(),
                'type'       => 1,
                'origin'     => 2,
                'payment'    => array(
                    'method' => 2,
                    'number' => $card_number,
                    'cvv'    => $card_cvv
                ),
                'expiration' => $card_expiration,
                'total'      => (int)$order->get_total(),
                'first'      => $order->get_billing_first_name(),
                'last'       => $order->get_billing_last_name(),
                'email'      => $order->get_billing_email(),
                'company'    => $order->get_billing_company(),
                'address1'   => $order->get_billing_address_1(),
                'address2'   => $order->get_billing_address_2(),
                'zip'        => $order->get_billing_postcode(),
                'state'      => $order->get_billing_state(),
                'city'       => $order->get_shipping_city()
            );

            return parent::handle_payment( $order_id, $data );
        }
        */

        /**
         * Process the payment and return the result
         *
         * @param int $order_id
         * @return array
         */
        public function process_payment( $order_id )
        {
            $order = wc_get_order($order_id);

            if( isset( $_POST[ '_transID' ] ) )
            {
                $trans_id = $_POST[ '_transID' ];
            }

            //Make sure transaction ID was set
            if( empty( $trans_id ) )
            {
                wc_add_notice( __( 'Transaction ID not found.', 'wc_splash_payments' ), 'error' );
                return array(
                    'result' => 'error',
                );
            }

            //Update WooCommerce transaction id to match the Splash transactions ID
            update_post_meta( $order_id, '_transaction_id', $trans_id );

            //Payment complete
            $order->payment_complete( $trans_id );

            //Add order note
            $complete_message = sprintf( __( 'Splash charge complete (Charge ID: %s).', 'wc_splash_payments' ), $trans_id );
            $order->add_order_note( $complete_message );

            //Remove cart
            WC()->cart->empty_cart();

            //Return thank you redirect
            return array(
                'result'   => 'success',
                'redirect' => $this->get_return_url( $order )
            );
        }

        /**
         * Refund a transaction.
         * @param int $order_id
         * @param float $amount
         * @param string $reason
         * @return bool
         */
        public function process_refund( $order_id, $amount = null, $reason = '' )
        {
            $order          = wc_get_order( $order_id );
            $transaction_id = $order->get_transaction_id();
            $data           = array(
                'type'   => 5,
                'fortxn' => $transaction_id,
                'first'  => wp_get_current_user()->first_name,
                'last'   => wp_get_current_user()->last_name,
                'total'  => $amount
            );

            return $this->handle_refund( $order, $transaction_id, $reason, $data );
        }
    }

endif;
